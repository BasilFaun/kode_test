FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8-alpine3.10
RUN apk add --no-cache --virtual .build-deps gcc libc-dev make postgresql-dev
RUN pip install --upgrade pip
COPY ./requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt
