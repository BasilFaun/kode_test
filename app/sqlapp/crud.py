from sqlalchemy import literal, func, alias
from sqlalchemy.orm import Session, subqueryload
from .models import User, Post, Attachment, Like
from ..schemas import (
    PostCreate,
    AnyHttpUrl
)


def get_user_by_email(db: Session, email: str):
    return db.query(User).filter_by(email=email).first()


def get_verified_user_by_email(db: Session, email: str):
    return db.query(User).filter_by(email=email, is_active=True).first()


def is_user_exists(db: Session, email: str):
    return db.query(literal(True)).filter(
        db.query(User).filter_by(email=email).exists()
    ).scalar()


def create_user(db: Session, email: str, pass_hash: str, email_salt: str):
    db_user = User(email=email, pass_hash=pass_hash, is_active=False, email_salt=email_salt)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def verify_user(db: Session, email: str):
    db.query(User).filter(User.email==email).update({"is_active": True, "email_salt": ""})
    db.commit()


def create_post(db: Session, user_id: int, post: PostCreate):
    db_post = Post(title=post.title, message=post.message, owner_id=user_id)
    db.add(db_post)
    db.commit()
    db.refresh(db_post)
    return db_post


def is_posts_owner(db: Session, user_id: int, post_id: int):
    return db.query(literal(True)).filter(
        db.query(Post).filter_by(id=post_id, owner_id=user_id).exists()
    ).scalar()


def create_attachment(db: Session, post_id: int, title: str, description: str, url: AnyHttpUrl):
    db_attachment = Attachment(post_id=post_id, title=title, description=description, url=url)
    db.add(db_attachment)
    db.commit()
    db.refresh(db_attachment)
    return db_attachment


def get_posts_list(db: Session):
    return db.query(Post.id, Post.title, Post.created_at, Post.owner_id)


def get_post(db: Session, post_id: int):
    return db.query(Post, func.count(Like.id)).\
        outerjoin(Post.likes).\
        filter(Post.id==post_id).\
        group_by(Post.id).\
        first()


def like_unlike_post(db: Session, user_id: int, post_id: int):
    ret = False
    like = db.query(Like).filter_by(user_id=user_id, post_id=post_id).first()
    if like:
        db.delete(like)
    else:
        like = Like(user_id=user_id, post_id=post_id)
        db.add(like)
        ret = True
    db.commit()
    return ret


def delete_post(db: Session, post_id: int):
    db.delete(db.query(Post).filter_by(id=post_id).first())
    db.commit()
