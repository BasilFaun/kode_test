from sqlalchemy import (
    Boolean,
    Column,
    ForeignKey,
    Integer,
    String,
    DateTime,
    text
)
from sqlalchemy.orm import relationship
from sqlalchemy.schema import UniqueConstraint
from .database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    email = Column(String, unique=True, index=True)
    pass_hash = Column(String)
    email_salt = Column(String)
    is_active = Column(Boolean, default=False)
    created_at = Column(DateTime, server_default=text("NOW()"))

    posts = relationship("Post", back_populates="owner")
    likes = relationship("Like", back_populates="user")


class Post(Base):
    __tablename__ = "posts"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    message = Column(String, index=True)
    created_at = Column(DateTime, server_default=text("NOW()"))
    owner_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))

    owner = relationship("User", back_populates="posts")
    attachments = relationship("Attachment", back_populates="post")
    likes = relationship("Like", back_populates="post")


class Attachment(Base):
    __tablename__ = "attachments"

    id = Column(Integer, primary_key=True, index=True)
    title = Column(String)
    description = Column(String)
    url = Column(String)
    created_at = Column(DateTime, server_default=text("NOW()"))
    post_id = Column(Integer, ForeignKey("posts.id", ondelete="CASCADE"))

    post = relationship("Post", back_populates="attachments")


class Like(Base):
    __tablename__ = "likes"
    __table_args__ = (UniqueConstraint('user_id', 'post_id', name='_user_post_uc'),)

    id = Column(Integer, primary_key=True, index=True)
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    post_id = Column(Integer, ForeignKey("posts.id", ondelete="CASCADE"))
    
    user = relationship("User", back_populates="likes")
    post = relationship("Post", back_populates="likes")
