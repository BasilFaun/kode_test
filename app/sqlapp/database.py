from fastapi import Request
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from os import getenv


POSTGRES_HOST = getenv("POSTGRES_HOST", "")
POSTGRES_PORT = getenv("POSTGRES_PORT", "")
POSTGRES_DB  = getenv("POSTGRES_DB", "")
POSTGRES_USER = getenv("POSTGRES_USER", "")
POSTGRES_PASSWORD = getenv("POSTGRES_PASSWORD", "")
DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_HOST}:{POSTGRES_PORT}/{POSTGRES_DB}"

engine = create_engine(DATABASE_URL)

SessionLocal = sessionmaker(autocommit=False, autoflush=True, bind=engine)

Base = declarative_base()

def get_db(request: Request):
    return request.state.db
