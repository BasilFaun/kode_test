from fastapi import (
    APIRouter,
    Depends,
    HTTPException,
    Query,
    status,
)
from passlib.hash import pbkdf2_sha256
from sqlalchemy.orm import Session
from ..auth import (
    calc_email_token,
    decodeJWT,
    JWTBearer,
    signJWT,
)
from ..sqlapp.database import get_db
from ..sqlapp.crud import (
    get_user_by_email,
    get_verified_user_by_email,
    create_user,
    is_user_exists,
    verify_user
)
from ..schemas import UserSign, Token
from ..utils import send_confirmation_email


router = APIRouter(
    prefix="/users",
    tags=["User"],
    responses={404: {"detail": "Not found"}}
)


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def user_create(user: UserSign, db: Session = Depends(get_db)):
    res = get_user_by_email(db, user.email)
    if res:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="User already exists")
    pass_hash = pbkdf2_sha256.hash(user.password)
    email_hash = pbkdf2_sha256.hash(user.email).split('$')
    create_user(db, user.email, pass_hash, email_hash[-2])
    await send_confirmation_email(user.email, email_hash[-1])
    return {"detail": "User created"}


@router.get("/verify")
async def user_verify(email: str = Query(...), key: str = Query(...), db: Session = Depends(get_db)):
    db_user = get_user_by_email(db, email)
    if db_user:
        token = calc_email_token(db_user.email, db_user.email_salt)
        if key==token:
            verify_user(db, db_user.email)
    return {"detail": "User verified"} #alway response 200ok for security

#raise HTTPException(status_code=400, detail=f"{}")

@router.post("/auth", response_model=Token)
async def user_auth(user: UserSign, db: Session = Depends(get_db)):
    db_user = get_verified_user_by_email(db, user.email)
    if db_user and pbkdf2_sha256.verify(user.password, db_user.pass_hash):
        return signJWT(db_user.email)
    raise HTTPException(
        status_code=status.HTTP_400_BAD_REQUEST,
        detail=f"Email or password is incorrect")


@router.post("/refresh", response_model=Token)
async def user_refresh(token: Token, db: Session = Depends(get_db)):
    payload = decodeJWT(token.access_token, is_refresh=True)
    if payload:
        email = payload.get("user_id")
        if is_user_exists(db, email):
            return signJWT(email)
    return {"access_token": ""}
