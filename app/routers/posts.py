from fastapi import (
    APIRouter,
    Depends,
    File, 
    UploadFile,
    HTTPException,
    Path,
    Query,
    status,
)
from fastapi_pagination import Page, pagination_params
from fastapi_pagination.ext.sqlalchemy import paginate
from shutil import copyfileobj
from pathlib import Path as FilePath
from sqlalchemy.orm import Session
from os import getenv
import re
from typing import Optional, Set
from ..auth import JWTBearer, decodeJWT
from ..sqlapp.crud import (
    get_posts_list,
    get_verified_user_by_email,
    create_post,
    create_attachment,
    get_post,
    is_posts_owner,
    like_unlike_post,
    delete_post
)
from ..sqlapp.database import get_db
from ..schemas import (
    PostShort,
    PostCreate,
    PostDetail,
    Attachment
)
from ..utils import get_opengraph_content


url_pattern = re.compile("http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+")

def get_current_user(token: str = Depends(JWTBearer()), db: Session = Depends(get_db)):
    payload = decodeJWT(token)
    if payload:
        db_user = get_verified_user_by_email(db, payload.get("user_id"))
        if not db_user:
            raise HTTPException(
                status_code=status.HTTP_401_UNAUTHORIZED,
                detail="User is not verified"
            )
        return db_user


router = APIRouter(
    prefix="/posts",
    tags=["Posts"],
    responses={status.HTTP_404_NOT_FOUND: {"detail": "Not found"}}
)


@router.get("", response_model=Page[PostShort], dependencies=[Depends(pagination_params)])
async def posts_list(db: Session = Depends(get_db)):
    return paginate(get_posts_list(db))


@router.post("/create", response_model=PostDetail, status_code=status.HTTP_201_CREATED)
async def post_create(
        post: PostCreate,
        user = Depends(get_current_user),
        db: Session = Depends(get_db)):
    db_post = create_post(db, user_id=user.id, post=post)
    for url in url_pattern.findall(post.message):
        og_content = await get_opengraph_content(url)
        if og_content:
            create_attachment(db, db_post.id, *og_content)
    return db_post


@router.get("/{post_id}", response_model=PostDetail)
async def post_detail(
        post_id: int = Path(..., title="ID of post to get"),
        db: Session = Depends(get_db)
    ):
    db_post = get_post(db, post_id)
    if db_post:
        post, likes_count = db_post
        setattr(post, 'likes_count', likes_count)
        return post
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="Post not found"
    )

@router.post("/{post_id}/uploadfile/", response_model=Attachment)
async def upload_file(
        post_id: int = Path(..., title="ID of post"),
        user = Depends(get_current_user),
        db: Session = Depends(get_db),
        upload_file: UploadFile = File(...)
    ):
    ct = upload_file.content_type.split('/')[0]
    if ct!="image" and ct!="video":
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail=f"Type file {ct} is not allowed"
        )
    if is_posts_owner(db, user.id, post_id):
        directory = FilePath(getenv('UPLOAD_DIR'))/FilePath(str(post_id))
        directory.mkdir(exist_ok=True)
        destination =  directory/FilePath(upload_file.filename)
        try:
            with destination.open("wb") as buf:
                copyfileobj(upload_file.file, buf)
        finally:
            upload_file.file.close()
        url = '/uploads/' + str(post_id) + '/' + upload_file.filename
        return create_attachment(db, post_id, upload_file.filename, "", url)
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User is not owner of this post"
    )


@router.get("/{post_id}/like")
async def post_like_unlike(
        post_id: int,
        user = Depends(get_current_user),
        db: Session = Depends(get_db)
    ):
    return {"like": like_unlike_post(db, user.id, post_id)}


@router.delete("/{post_id}", status_code=status.HTTP_204_NO_CONTENT)
async def post_delete(
        post_id: int = Path(..., title="ID of post"),
        user = Depends(get_current_user),
        db: Session = Depends(get_db)
    ):
    if is_posts_owner(db, user.id, post_id):
        delete_post(db, post_id)
        return {"detail": f"Post {post_id} deleted"}
    raise HTTPException(
        status_code=status.HTTP_403_FORBIDDEN,
        detail="User is not owner of this post"
    )
