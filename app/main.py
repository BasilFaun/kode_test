import sys
from fastapi import FastAPI, Request, Response
from .sqlapp.database import Base, SessionLocal, engine
from .routers import posts, users

Base.metadata.create_all(bind=engine)

app = FastAPI()

@app.middleware("http")
async def db_session_middleware(request: Request, call_next):
    response = Response("Internal server error", status_code=500)
    try:
        request.state.db = SessionLocal()
        response = await call_next(request)
    finally:
        request.state.db.close()
    return response


version = f"{sys.version_info.major}.{sys.version_info.minor}"

@app.get("/", tags=["Root"])
async def index():
    return {"message": f"Hello, Ololo! This blog run on Python {version}."}

app.include_router(users.router, prefix="/api")
app.include_router(posts.router, prefix="/api")
