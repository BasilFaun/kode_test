import asyncio
import aiosmtplib
from os import getenv
from html.parser import HTMLParser
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import httpx


async def send_mail_async(sender, to, subject, text, textType='plain', **params):
    """Send an outgoing email with the given parameters.

    :param sender: From whom the email is being sent
    :type sender: str

    :param to: A list of recipient email addresses
    :type to: list

    :param subject: The subject of the email
    :type subject: str

    :param text: The text of the email
    :type text: str

    :param textType: Mime subtype of text, defaults to 'plain' (can be 'html')
    :type text: str

    :param params: An optional set of parameters. (See below)
    :type params: dict

    Optional Parameters:
    :cc: A list of Cc email addresses.
    :bcc: A list of Bcc email addresses.
    """

    # Default Parameters
    cc = params.get("cc", [])
    bcc = params.get("bcc", [])

    # Prepare Message
    msg = MIMEMultipart()
    msg.preamble = subject
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ', '.join(to)
    if len(cc): msg['Cc'] = ', '.join(cc)
    if len(bcc): msg['Bcc'] = ', '.join(bcc)

    msg.attach(MIMEText(text, textType, 'utf-8'))

    # Contact SMTP server and send Message
    host = getenv('EMAIL_HOST', 'localhost')
    isSSL = getenv('EMAIL_SSL', False);
    isTLS = getenv('EMAIL_TLS', False);
    port = getenv('EMAIL_PORT', 465 if isSSL else 25)
    user = getenv('EMAIL_USER', '')
    password = getenv('EMAIL_PASSWORD', '')
    smtp = aiosmtplib.SMTP(hostname=host, port=port, use_tls=isSSL)
    await smtp.connect()
    if isTLS:
        await smtp.starttls()
    if user:
        await smtp.login(user, password)
    await smtp.send_message(msg)
    await smtp.quit()


async def send_confirmation_email(email: str, token: str):
    sender = getenv("EMAIL_SENDER", "")
    hostname = getenv("HOSTNAME", "")
    subject = f"Verify your account on {hostname}"
    text = f"Follow link to verify your email {hostname}/api/users/verify?email={email}&key={token}"
    await send_mail_async(sender, [email], subject, text)


class MyHTMLParser(HTMLParser):
    metadata = dict()

    def handle_startendtag(self, tag, attrs):
        if tag=="meta":
            name = None
            content = None
            for attr, val in attrs:
                if attr=="property":
                    name = val
                if attr=="content":
                    content = val
            if name and content:
                self.metadata[name] = content


async def get_opengraph_content(url: str):
    """
    <meta property="og:title" content="Text">
    <meta property="og:description" content="Text">
    <meta property="og:image" content="http://example.com/thumbnail.jpg">
    """

    async with httpx.AsyncClient() as client:
        r = await client.get(url)
        if r.status_code==200:
            parser = MyHTMLParser()
            parser.feed(r.text)
            return (
                parser.metadata.get("og:title"),
                parser.metadata.get("og:description"),
                parser.metadata.get("og:image"),
            )
