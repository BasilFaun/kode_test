from fastapi import Request, HTTPException
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
import jwt
from os import getenv
from passlib.hash import pbkdf2_sha256
from passlib.utils import ab64_decode
from time import time
from typing import Dict


JWT_SECRET = getenv("JWT_SECRET", "")
JWT_ALGORITHM = getenv("JWT_ALGORITHM", "HS256")
JWT_LIFETIME = float(getenv("JWT_LIFETIME", 600))
JWT_REFRESH_LIFETIME = float(getenv("JWT_REFRESH_LIFETIME", 604800))


def signJWT(user_id: str) -> Dict[str, str]:
    payload = {
        "user_id": user_id,
        "expires": time() + JWT_LIFETIME
    }
    token = jwt.encode(payload, JWT_SECRET, algorithm=JWT_ALGORITHM)
    return {"access_token": token}


def decodeJWT(token: str, is_refresh: bool = False) -> dict:
    try:
        decoded_token = jwt.decode(token, JWT_SECRET, algorithms=[JWT_ALGORITHM])
    except:
        return
    expires = decoded_token["expires"]
    if is_refresh:
        expires = decoded_token["expires"]-JWT_LIFETIME+JWT_REFRESH_LIFETIME
    return decoded_token if expires >= time() else None



class JWTBearer(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super(JWTBearer, self).__init__(auto_error=auto_error)

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials = await super(JWTBearer, self).__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=401, detail="Invalid authentication scheme")
            if not self.verify_jwt(credentials.credentials):
                raise HTTPException(status_code=401, detail="Invalid or expired token")
            return credentials.credentials
        else:
            raise HTTPException(status_code=401, detail="Invalid authorization code")

    def verify_jwt(self, jwtoken: str) -> bool:
        isTokenValid: bool = False
        payload = decodeJWT(jwtoken)
        if payload:
            isTokenValid = True
        return isTokenValid


def calc_email_token(email: str, salt: str):
    return pbkdf2_sha256.using(salt=ab64_decode(salt)).hash(email).split('$')[-1]
