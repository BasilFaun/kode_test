from datetime import datetime
from pydantic import BaseModel, Field, EmailStr, AnyHttpUrl
from typing import Optional, List


class UserSign(BaseModel):
    email: EmailStr = Field(description="User email")
    password: str = Field(description="User passwords")


class Token(BaseModel):
    access_token: str = Field(description="Access JWT token")


class PostCreate(BaseModel):
    title: str = Field(description="Post title")
    message: Optional[str] = Field(None, description="Post text")


class PostShort(BaseModel):
    id: int = Field(gt=0, description="Post ID")
    title: str = Field(description="Post title")
    created_at: datetime = Field(description="Post creation date and time")
    owner_id: int = Field(description="Post owner")

    class Config:
        orm_mode = True


class Attachment(BaseModel):
    id: str = Field(description="Attachment id")
    title: str = Field(None, description="Title of file")
    description: str = Field(None, description="Title of file")
    url: str = Field(description="Url of file")

    class Config:
        orm_mode = True


class PostDetail(BaseModel):
    id: int = Field(gt=0, description="Post ID")
    title: str = Field(description="Post title")
    message: Optional[str] = Field(None, description="Post text")
    created_at: datetime = Field(description="Post creation date and time")
    owner_id: int = Field(description="Post owner")
    likes_count: int = Field(0, description="Likes count")
    attachments: Optional[List[Attachment]] = Field(description="Attachment files")
    
    class Config:
        orm_mode = True
