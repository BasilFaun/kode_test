location / {
    try_files $uri @proxy_to_app;
}

location @proxy_to_app {
    include /etc/nginx/conf.d/gunicorn_params.inc;
}

location /uploads {
    root /opt/;
}

error_page 500 502 503 504 /50x.html;

location = /50x.html {
    root /usr/share/nginx/html/;
}
