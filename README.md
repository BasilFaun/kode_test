# Simple blog on FastAPI
This is simple blog application based on [FastAPI framework](https://fastapi.tiangolo.com)

## Installation
Install [docker and docker-compose](https://www.docker.com/) 
```
sudo apt install docker docker-compose -y
```

Clone project to your directory
```
git clone git@bitbucket.org:BasilFaun/kode_test.git 
```

Create auxiliary directories
```
mkdir -p data log/app log/nginx tmp uploads
```

Create environment .env file
```
cp .env.example .env
```

Build docker images and containers
```
docker-compose up -d
```

Now you can pass to [url](http://127.0.0.1:8080/docs)
